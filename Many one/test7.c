#define _GNU_SOURCE
#include<stdio.h>
#include<stdlib.h>
#include<ucontext.h>
#include<signal.h>
#include<unistd.h>
#include <sys/mman.h>
#include <linux/sched.h>
#include <sched.h>
#include <syscall.h>
#include <sys/time.h>
#include <string.h>
#include <stdatomic.h>
#include <stdbool.h>
#include "threads.h"


void func(void* thread_id){
    return;
}

// FOR TESTING THREAD_JOIN FUNCTION
int main(int argc, char const *argv[])
{  
    printf("\n--------------SIMPLE TESTING 7 ---------------------\n"); 
    printf("\nEXPECTED 101 is not found as it is not created");
    int total_thread_created = 0;
    for( int i = 0 ; i < 4 ; i++ ){
        total_thread_created += ( 0 == mythread_create(func,NULL));
    }
    printf("\n%d\n",mythread_join(101));
    return 0;
}