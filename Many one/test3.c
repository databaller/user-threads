#define _GNU_SOURCE
#include<stdio.h>
#include<stdlib.h>
#include<ucontext.h>
#include<signal.h>
#include<unistd.h>
#include <sys/mman.h>
#include <linux/sched.h>
#include <sched.h>
#include <syscall.h>
#include <sys/time.h>
#include <string.h>
#include <stdatomic.h>
#include <stdbool.h>
#include "threads.h"

void func1(void* thread_id){
    printf("\nTHREAD0 IS RUNNING and its going in while loop\n");
    int l = 0;
    while(l < 1000){
        //printf("\n00000\n");
        l++;
    }
    printf("\nwhile loop of THREAD0 over\n");
    return;
}

void func2(void* thread_id){
    printf("\nTHREAD1 IS RUNNING and its going in while loop\n");
    int l = 0;
    while(l < 1000){
        //printf("\n11111\n");
        l++;
    }
    printf("\nwhile loop of THREAD1 over\n");
    return;
}

void func3(void* thread_id){
    printf("\nTHREAD2 IS RUNNING and its going in while loop\n");
    int l = 0;
    while(l < 1000){
        //printf("\n22222\n");
        l++;
    }
    printf("\nwhile loop of THREAD2 over\n");
    return;
}

int main(int argc, char const *argv[])
{
    printf("\n--------------SIMPLE TESTING 3 ---------------------\n");
    mythread_create(func1,NULL);
    mythread_create(func2,NULL);
    mythread_create(func3,NULL);
    mythread_join(0);
    mythread_join(1);
    mythread_join(2);
    return 0;
}