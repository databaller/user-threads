#define _GNU_SOURCE
#include<stdio.h>
#include<stdlib.h>
#include<ucontext.h>
#include<signal.h>
#include<unistd.h>
#include <sys/mman.h>
#include <linux/sched.h>
#include <sched.h>
#include <syscall.h>
#include <sys/time.h>
#include <string.h>
#include <stdatomic.h>
#include <stdbool.h>
#include "threads.h"

void func1(void* thread_id){
    printf("\nTHREAD0 IS RUNNING and its going to sleep for 10 seconds\n");
    sleep(10);
    printf("\nTHREAD0 woke after 10 seconds\n");
    return;
}

void func2(void* thread_id){
    printf("\nTHREAD1 IS RUNNING  and its going to sleep for 5 seconds\n");
    sleep(5);
    printf("\nTHREAD0 woke after 5 seconds\n");
    return;
}

void func3(void* thread_id){
    printf("\nTHREAD2 IS RUNNING and its going to sleep for 3 seconds\n");
    sleep(3);
    printf("\nTHREAD0 woke after 3 seconds\n");
    return;
}

int main(int argc, char const *argv[])
{
    printf("\n--------------SIMPLE TESTING 2 ---------------------\n");
    mythread_create(func1,NULL);
    mythread_create(func2,NULL);
    mythread_create(func3,NULL);
    mythread_join(0);
    mythread_join(1);
    mythread_join(2);
    return 0;
}