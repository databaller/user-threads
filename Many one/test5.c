#define _GNU_SOURCE
#include<stdio.h>
#include<stdlib.h>
#include<ucontext.h>
#include<signal.h>
#include<unistd.h>
#include <sys/mman.h>
#include <linux/sched.h>
#include <sched.h>
#include <syscall.h>
#include <sys/time.h>
#include <string.h>
#include <stdatomic.h>
#include <stdbool.h>
#include "threads.h"


void func(void* thread_id){
    return;
}

// FOR TESTING THREAD_CREATE FUNCTION
int main(int argc, char const *argv[])
{   
    printf("\n--------------SIMPLE TESTING 5 ---------------------\n");
    printf("\nEXPECTED TOTAL THREADS CREATED :%d\n",100);
    int total_thread_created = 0;
    for( int i = 0 ; i < 100 ; i++ ){
        total_thread_created += ( 0 == mythread_create(func,NULL));
    }
    printf("\nTOTAL THREADS CREATED : %d\n",total_thread_created);
    return 0;
}