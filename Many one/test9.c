#define _GNU_SOURCE
#include<stdio.h>
#include<stdlib.h>
#include<ucontext.h>
#include<signal.h>
#include<unistd.h>
#include <sys/mman.h>
#include <linux/sched.h>
#include <sched.h>
#include <syscall.h>
#include <sys/time.h>
#include <string.h>
#include <stdatomic.h>
#include <stdbool.h>
#include "threads.h"

int final_ans = 1;
int current_number = 10;

void func(void* val_ptr){
    printf("\nFINAL ANSWER :%d & CURRENT NUMBER: %d\n",final_ans,current_number);
    while(current_number > 0){
        final_ans = final_ans * current_number;
        current_number = current_number - 1;
        sleep(2);
    }
    return;
}

// test for checking argument passing
int main(int argc, char const *argv[])
{   
    printf("\n--------------SIMPLE TESTING 9 ---------------------\n");
    mythread_create(func,NULL);
    mythread_create(func,NULL);
    mythread_create(func,NULL);
    mythread_join(0);
    mythread_join(1);
    mythread_join(2);
    return 0;
}