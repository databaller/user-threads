#define _GNU_SOURCE
#include<stdio.h>
#include<stdlib.h>
#include<ucontext.h>
#include<signal.h>
#include<unistd.h>
#include <sys/mman.h>
#include <linux/sched.h>
#include <sched.h>
#include <syscall.h>
#include <sys/time.h>
#include <string.h>
#include <stdatomic.h>
#include <stdbool.h>

#define RUNNING 100           // CPU is currently executing this thread
#define READY 101             // CPU can start executing this thread
#define BLOCKED 102           // thread is waiting for IO event to occur
#define SUSPENDED 103         // it is paused temporarily
#define TERMINATED 104        // its executed by CPU
#define SLEEPING   105
#define STACK_SIZE 65536      // stack size
#define START_TID 1           // start tid

typedef void (*funcptr)(void *); 

typedef struct thread
{
    int tid;    // thread id
    void* stack_ptr;    // pointer to stack of thread
    int stack_size; // size of stack used by thread
    funcptr func;  // function executed by thread
    void* args; // arguments passed to thread
    void* return_value; // return value use f
    ucontext_t* context_ptr; // context ( very important)
    int state;
    struct thread* next;
    void* n;
    sigset_t signals;
} thread;

// Mutual exclusion lock.
typedef struct my_spinlock {
    int is_locked;       // Indicates whether the lock is held
    int owner;          // ID of the thread that currently holds the lock
} my_spinlock;

// Long-term locks for processes
typedef struct my_sleeplock {
    int is_locked;        // Indicates whether the lock is held
    my_spinlock spin_lock; // Spinlock used to protect this sleep lock
    int owner;   // ID of the thread that currently holds the lock
} my_sleeplock;

// for checking alarms
struct sigaction alarm_res; 

// function for initializing spin lock
void my_spinlock_init(my_spinlock *lock);

// function for locking the lock
void my_spinlock_lock(my_spinlock *lock, int thread_id);

// function unlocking lock
void my_spinlock_unlock(my_spinlock *lock) ;

// initializing sleep lock
void sleep_lock_init(my_sleeplock *lock);

// starting sleep lock
void sleep_lock_lock(my_sleeplock *lock, int owner) ;

// wakeup from sleep lock
void sleep_lock_unlock(my_sleeplock *lock);

// function for stoping the timer
void stop_alarm();

// function for starting the timer
void begin_alarm();

// Add thread to the linked list
void add_thread(thread* new_thread);

// Delete thread from the linked list
void delete_thread(int tid);

// find thread with given thread it
thread* find_thread(int tid);

// function to print all threads
void print_all_threads();

// code for finding current running threads
int count_running_threads();

// cdoe for scheduler
void scheduler();

// function for choosing thread from linked list
thread *choose_ready_thread() ;

// function for finding running thread from queue
thread *find_running_thread();

// exiting from thread
void thread_exit();

// function which runs the function of that thread with that arguments
void start_func(thread* thread_ptr);

// function for many one initialization
void many_one_init();

// function for creating thread
int mythread_create(funcptr f, void *args) ;

// waiting thread to finish
int mythread_join(int tid);
