#define _GNU_SOURCE
#include<stdio.h>
#include<stdlib.h>
#include<ucontext.h>
#include<signal.h>
#include<unistd.h>
#include <sys/mman.h>
#include <linux/sched.h>
#include <sched.h>
#include <syscall.h>
#include <sys/time.h>
#include <string.h>
#include <stdatomic.h>
#include <stdbool.h>
#include "threads.h"

int final_ans = 0;

void func1(void* thread_id){
    printf("\nbefore incrementing in thread0 : %d\n",final_ans);
    final_ans++;
    printf("\nafter incrementing in thread0 : %d\n",final_ans);
    return;
}

void func2(void* thread_id){
    printf("\nbefore incrementing in thread1 : %d\n",final_ans);
    final_ans++;
    printf("\nafter incrementing in thread1 : %d\n",final_ans);
    return;
}

void func3(void* thread_id){
    printf("\nbefore incrementing in thread2 : %d\n",final_ans);
    final_ans++;
    printf("\nafter incrementing in thread2 : %d\n",final_ans);
    return;
}


// test wrt to computation of global variables
int main(int argc, char const *argv[])
{  
    printf("\n--------------SIMPLE TESTING 4 ---------------------\n"); 
    printf("\nEXPECTED VALUE :%d\n",3);
    mythread_create(func1,NULL);
    mythread_create(func2,NULL);
    mythread_create(func3,NULL);
    mythread_join(0);
    mythread_join(1);
    mythread_join(2);
    return 0;
}