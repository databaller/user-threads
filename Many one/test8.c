#define _GNU_SOURCE
#include<stdio.h>
#include<stdlib.h>
#include<ucontext.h>
#include<signal.h>
#include<unistd.h>
#include <sys/mman.h>
#include <linux/sched.h>
#include <sched.h>
#include <syscall.h>
#include <sys/time.h>
#include <string.h>
#include <stdatomic.h>
#include <stdbool.h>
#include "threads.h"

int final_ans = 0;

void func(void* val_ptr){
    printf("\nPrinting value: %d\n",*(int*)val_ptr);
    return;
}

// test for checking argument passing
int main(int argc, char const *argv[])
{   
    printf("\n--------------SIMPLE TESTING 8 ---------------------\n");
    int val1 = 100;
    int val2 = 101;
    int val3 = 102;
    mythread_create(func,&val1);
    mythread_create(func,&val2);
    mythread_create(func,&val3);
    mythread_join(0);
    mythread_join(1);
    mythread_join(2);
    return 0;
}