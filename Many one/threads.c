#define _GNU_SOURCE
#include<stdio.h>
#include<stdlib.h>
#include<ucontext.h>
#include<signal.h>
#include<unistd.h>
#include <sys/mman.h>
#include <linux/sched.h>
#include <sched.h>
#include <syscall.h>
#include <sys/time.h>
#include <string.h>
#include <stdatomic.h>
#include <stdbool.h>
#include "threads.h"


// is many : one done ?
int many_one_done = 0;

thread scheduler_thread;
thread* current_running_thread = NULL;
thread* thread_to_save;
thread* thread_to_sched;

int global_tid = 0;

// head of linked list of thread
thread* head = NULL;

// function for initializing spin lock
void my_spinlock_init(my_spinlock *lock) {
    lock->is_locked = 0;
    lock->owner = -1;
}

// function for locking the lock
void my_spinlock_lock(my_spinlock *lock, int thread_id) {
    while (__sync_lock_test_and_set(&lock->is_locked, 1)) {
        // spin until lock is acquired
    }
    lock->owner = thread_id;
}

// function unlocking lock
void my_spinlock_unlock(my_spinlock *lock) {
    lock->owner = -1;
    __sync_lock_release(&lock->is_locked);
}

void sleep_lock_init(my_sleeplock *lock) {
    lock->is_locked = 0;
    my_spinlock_init(&lock->spin_lock);
    lock->owner = -1;
}

void sleep_lock_lock(my_sleeplock *lock, int owner) {
    my_spinlock_lock(&lock->spin_lock, owner);
    while (lock->is_locked) {
        // Release the spin lock before sleeping
        my_spinlock_unlock(&lock->spin_lock);

        // Sleep until the lock is released
        // Assumes that there is a function called sleep() that sleeps for a specified number of seconds
        sleep(1);

        // Reacquire the spin lock before checking the lock again
        my_spinlock_lock(&lock->spin_lock, owner);
    }
    lock->is_locked = 1;
    lock->owner = owner;
    my_spinlock_unlock(&lock->spin_lock);
}

void sleep_lock_unlock(my_sleeplock *lock) {
    my_spinlock_unlock(&lock->spin_lock);
    lock->is_locked = 0;
    lock->owner = -1;
    my_spinlock_unlock(&lock->spin_lock);
}

// function for stoping the timer
void stop_alarm(){
    struct itimerval alarm;
    alarm.it_value.tv_sec = 0;
    alarm.it_value.tv_usec = 0;
    alarm.it_interval.tv_sec = 0;
    alarm.it_interval.tv_usec = 0;
    setitimer(ITIMER_REAL,&alarm,NULL);
}

// function for starting the timer
void begin_alarm(){
    struct itimerval alarm;
    alarm.it_value.tv_sec = 0;
    alarm.it_value.tv_usec = 15000;
    alarm.it_interval.tv_sec = 0;
    alarm.it_interval.tv_usec = 15000;
    setitimer(ITIMER_REAL, &alarm, NULL);
}

// Add thread to the linked list
void add_thread(thread* new_thread) {
    // if the list is empty, add the new node as the head
    if (head == NULL) {
        head = new_thread;
        return;
    }

    // find the last node in the list
    thread* curr = head;
    while (curr->next != NULL) {
        curr = curr->next;
    }

    // add the new node to the end of the list
    curr->next = new_thread;
}

// Delete thread from the linked list
void delete_thread(int tid) {
    thread *current_thread = head;  // current traverser
    thread *prev_thread = NULL;     // previous traverser
    while (current_thread != NULL && current_thread->tid != tid) { // till you dont find thread to be deleted       
        prev_thread = current_thread;   // update previous thread       
        current_thread = current_thread->next;  // update current thread   
    }
    if (current_thread == NULL) {
        return; // thread not found in the linked list
    }
    if (prev_thread == NULL) {
        head = current_thread->next; // deleting the head
    } else {
        prev_thread->next = current_thread->next; // deleting from the middle or end    
    }
    free(current_thread->stack_ptr);
    free(current_thread->context_ptr);
    free(current_thread);   // take the memory back
}

thread* find_thread(int tid) {
    thread *current_thread = head;  // current thread pointer
    while (current_thread != NULL && current_thread->tid != tid) { // till you dont find thread that you want        
        current_thread = current_thread->next;  // update the current thread
    }
    return current_thread; // returns NULL if thread not found in the linked list
}

// function to print all threads
void print_all_threads() {
    thread *current_thread = head;      // traverser
    printf("\n");                       // start
    while (current_thread != NULL) {    // till end
        printf("[ thread_id : %d ; state : %d] -> ",current_thread->tid,current_thread->state);    // print content
        current_thread = current_thread->next;  // update current pointer
        if( current_thread == NULL){
            printf(" NULL");
        }
    }
    printf("\n");   // end
}

int count_running_threads(){
    thread* curr = head;
    int count = 0;
    while(curr){
        count += ( RUNNING == curr->state);
        curr = curr -> next;
    }
    return count;
}

void scheduler() {
    //printf("\nScheduler is invoked after thread%d\n",current_running_thread->tid);
    stop_alarm();
    if( head -> next == NULL){
        //printf("\nAll threads executed !!!\n");
        exit(0);
    }
    //print_all_threads();
    if(current_running_thread->state == RUNNING){
        //printf("\nthread%d is RUNNING now it is READY\n",current_running_thread->tid);
        current_running_thread -> state = READY;
        //print_all_threads();
    }
    thread_to_save = current_running_thread;
    thread* nxt_thread_to_run = current_running_thread->next;
    if(!nxt_thread_to_run){
        nxt_thread_to_run = head;
    }
    //printf("\nFinding runnable thread\n");
    while (nxt_thread_to_run->state != READY)
    {
        if(nxt_thread_to_run->next) nxt_thread_to_run = nxt_thread_to_run->next;
        else nxt_thread_to_run = head;
    }
    nxt_thread_to_run->state = RUNNING;
    current_running_thread = nxt_thread_to_run;
    //printf("\nSaving thread%d and jumping to thread%d\n",thread_to_save->tid,nxt_thread_to_run->tid);
    begin_alarm();
    swapcontext(thread_to_save->context_ptr,nxt_thread_to_run->context_ptr); 
}

// function for choosing thread from linked list
thread *choose_ready_thread() {
    thread *current = head;                  // choose the traverser
    while (current != NULL) {                // traverse till end
        if (current->state == READY) {       // if thread is ready
            return current;                  // return ready thread
        }
        current = current->next;             // update the current thread
    }
    return NULL;                             // return null
}

// function for finding running thread from queue
thread *find_running_thread() {
    thread *current = head;                         // choose the traverser
    while (current != NULL) {                       // traverse till end
        if (current->state == RUNNING) {            // if thread is running
            return current;                         // return ready thread
        }
        current = current->next;                    // update the current thread
    }
    return NULL;                            // return null
}

void thread_exit(){
    stop_alarm();
    current_running_thread->state = TERMINATED;
    thread* temp = head;
    while(temp){
        if( temp -> state == TERMINATED && temp->tid != 1000){
            //printf("\nTHREAD%d IS TERMINATED & DELETED\n",current_running_thread->tid);
            delete_thread(temp->tid);
        }
        temp = temp -> next;
    }
    begin_alarm();
    scheduler();
}

// function which runs the function of that thread with that arguments
void start_func(thread* thread_ptr){
    thread_ptr->func(thread_ptr->args);
    thread_exit();
}

// function for many one initialization
void many_one_init(){

    scheduler_thread.context_ptr = (ucontext_t*) malloc(sizeof(ucontext_t));    // allocate the memory for storing the context of scheduelr thread
    scheduler_thread.stack_ptr = malloc(sizeof(STACK_SIZE));                    // allocate the memory for stack of scheduelr thread
    scheduler_thread.stack_size = STACK_SIZE;                                   // specify the size of stack
    scheduler_thread.func = scheduler;                                          // function which will be executed by thread
    scheduler_thread.return_value = NULL;                                       // scheduler will point to nothing
    getcontext(scheduler_thread.context_ptr);                                   // save the context of scheduler thread
    scheduler_thread.context_ptr->uc_stack.ss_sp = scheduler_thread.stack_ptr;  // after setcontext on scheduler thread use this stack pointer
    scheduler_thread.context_ptr->uc_stack.ss_size = STACK_SIZE;                // after setcontext on scheduler thread use this stack size
    makecontext(scheduler_thread.context_ptr,(void*)scheduler_thread.func,0);   // after setcontext on scheduler thread use this instruction pointer
    
    thread* main_thread_node = (thread*) malloc(sizeof(thread));                // thread which will run main function ( when no thread in linked list to avoid error )
    main_thread_node->state = RUNNING;                                          // it will running
    main_thread_node->args = NULL;                                              // it has no arguments
    main_thread_node->context_ptr = (ucontext_t*) malloc(sizeof(ucontext_t));   // allocate the memory for storing the context of main thread
    main_thread_node->return_value = 0;                                         // it has zero as return value
    main_thread_node->stack_ptr=NULL;                                           // no stack size required for main thread
    main_thread_node->stack_size = 0;                                           // no stack size required for main thread                  
    main_thread_node->func = NULL;                                              // main thread is pointing to no function
    main_thread_node->tid = 1000;                                               // its thread id i 1000
    getcontext(main_thread_node->context_ptr);                                  // save context of main thread
    add_thread(main_thread_node);                                               // add main thread in linked list
    current_running_thread = main_thread_node;                                  // not current running thread is main thread
    current_running_thread -> state = RUNNING;                                  // set its state as running
    
    memset(&alarm_res,0,sizeof alarm_res);                                      // when there alarm scheduler function will run                 
    alarm_res.sa_handler = scheduler;                                            
    alarm_res.sa_flags = SA_NODEFER;                                            
    sigaction(SIGALRM,&alarm_res,0);                                            

}

// function for creating thread
int mythread_create(funcptr f, void *args) {
	stop_alarm();   // stop the alarm
    if( many_one_done == 0 ){                                               // if many one init is not done
        many_one_init();                                                    // then do it
        many_one_done = 1;                                                  // update the status 
    }
    thread* new_thread = (thread*) malloc(sizeof(thread));                  // allocate the memory for structure of thread
    new_thread->tid = global_tid++;                                         // give it unique identification
    new_thread->stack_ptr = malloc(STACK_SIZE);                             // allocate size for its stack
    new_thread->stack_size = STACK_SIZE;                                    // specify its stack
    new_thread->func = f;                                                   // specify which function it will run
    new_thread->args = args;                                                // specify its arguments
    new_thread->context_ptr = (ucontext_t*) malloc(sizeof(ucontext_t));     // allocate memory for its context information
    new_thread->state = READY;                                              // make it ready
    getcontext(new_thread->context_ptr);                                    // save its context
    new_thread->context_ptr->uc_stack.ss_sp = new_thread->stack_ptr;        // after setcontext to it specify which stack will be used
    new_thread->context_ptr->uc_stack.ss_size = STACK_SIZE;                 // after setcontext to it specify what stack size will be used
    makecontext(new_thread->context_ptr,(void*)&start_func,1,new_thread);   // change the location of instruction pointer to start func
	add_thread(new_thread);                                                 // add thread in linked list
    begin_alarm();                                                          // start alarm
    return 0;                                                               // if successfull return 0
}



int mythread_join(int tid){
    thread* wait_thread = find_thread(tid);    // find the thread with that tid
    if (!wait_thread){                         //
        printf("\nThread not found\n");
        return 404;
    }
    while( wait_thread -> state != TERMINATED ) // wait till it is terminated
    ;
    return 0;                                   // after it is terminated clean it
}


// // function for killing thread
// int thread_kill(int tid, int input){
//     stop_timer();
//     if(input < 0 || input > 64){
//         begin_timer();
//         return 404;
//     }
//     if(current_running_thread->tid== tid){
//         raise(input);
//         begin_alarm();
//         return 0;
//     }
//     if(input == SIGSTOP || input == SIGCONT || input == SIGINT){
//         kill(getpid(), input);
//     }
//     else{
//         thread* thread_to_signal = find_thread(tid);
//         if(!thread_to_signal){
//             begin_timer();
//             return 404;
//         }
//         sigaddset(&thread_to_signal->signals, input);
//     }
//     begin_alarm();
//     return 0;
// }